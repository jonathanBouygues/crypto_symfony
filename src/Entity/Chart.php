<?php

namespace App\Entity;

use App\Repository\ChartRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChartRepository::class)]
class Chart
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'date')]
    private $created_at;

    #[ORM\Column(type: 'float')]
    private $price;

    #[ORM\ManyToOne(targetEntity: Token::class, inversedBy: 'charts')]
    private $symbol;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSymbol(): ?Token
    {
        return $this->symbol;
    }

    public function setSymbol(?Token $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    public function jsonSerialize(): array
    {
        return array(
            'date' => $this->getCreatedAt()->format("Y-m-d"),
            'price' => $this->getPrice(),
            'symbol' => $this->getSymbol()->getSlug(),
        );
    }
}
