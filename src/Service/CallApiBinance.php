<?php

namespace App\Service;

class CallApiBinance
{
    public function getCurrentValue(array $tokens): array
    {
        // initialize the data
        $currentValue = (float) 0;

        // Loop on token
        foreach($tokens as $key => $value) {
            // Initialize the curl connection curl
            $ch = curl_init();
            // Set the curl options
            curl_setopt($ch, CURLOPT_URL, "https://api.binance.com/api/v3/avgPrice?symbol=" . strtoupper($key));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            // Send the request
            $result_current_value = curl_exec($ch);
            $currentValue = json_decode($result_current_value, true);
            $tokens[$key] = $currentValue['price'];
            // Close the connection
            curl_close($ch);
        }

        return $tokens;
    }
}
