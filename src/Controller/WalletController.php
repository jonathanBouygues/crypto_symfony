<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TradeRepository;

class WalletController extends AbstractController
{
    #[Route('/wallet', name: 'wallet')]
    public function index(TradeRepository $tradeRepository): Response
    {   

        return $this->render('wallet/index.html.twig', [
            'controller_name' => 'Wallet',
            'trades' => $tradeRepository->findBy(['user' => $this->getUser()]),
        ]);
    }
}
