<?php

namespace App\Controller;

use App\Entity\Token;
use App\Form\TokenType;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


#[Route('/admin')]
class AdminController extends AbstractController
{
    #[Route('/', name: 'admin')]
    public function index(UserRepository $userRepository, TokenRepository $tokenRepository): Response
    {
        return $this->render('admin/index.html.twig', [
            'users' => $userRepository->findAll(),
            'tokens' => $tokenRepository->findAll(),
        ]);
    }

    #[Route('/new_token', name: 'newToken')]
    public function newToken(Request $request, EntityManagerInterface $em) : Response 
    {
        $token = new Token();
        $form = $this->createForm(TokenType::class, $token);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($token);
            $em->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/newToken.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}/edit', name: 'token_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Token $token, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TokenType::class, $token);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/editToken.html.twig', [
            'token' => $token,
            'form' => $form,
        ]);
    }

    #[Route('/delete/{id}/{csrf}', name: 'token_delete', methods: ['GET'])]
    public function delete(Request $request, Token $token, EntityManagerInterface $entityManager): Response
    {
        $csrf = $request->attributes->get('csrf');

        if ($this->isCsrfTokenValid('delete', $csrf)) {
            $entityManager->remove($token);
            $entityManager->flush();
        } 

        return $this->redirectToRoute('admin', [], Response::HTTP_SEE_OTHER);
    }

}
