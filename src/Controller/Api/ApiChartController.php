<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ChartRepository;


class ApiChartController extends AbstractController
{
    #[Route('/api/charts', name: 'api_charts')]
    public function charts(ChartRepository $chartRepository): Response
    {
        $charts = $chartRepository->findBy(array(), array('created_at' => 'ASC'));
        $charts_array = [];

        foreach ($charts as $chart) {
            array_push($charts_array, $chart->jsonSerialize());
        }

        $data = json_encode($charts_array);
        
        $response = new Response();
        $response->setContent($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
