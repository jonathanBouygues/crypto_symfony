<?php

namespace App\Controller\Api;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ApiUserController extends AbstractController
{
    
    #[Route('/api/users', name:'api_users')]
    public function users(UserRepository $userRepository) : Response
    {
        $users = $userRepository->findAll();
        $users_array = [];

        foreach ($users as $user) {
            array_push($users_array, $user->jsonSerialize());
        }

        $data = json_encode($users_array);
        
        $response = new Response();
        $response->setContent($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
