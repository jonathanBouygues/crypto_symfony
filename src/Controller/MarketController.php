<?php

namespace App\Controller;

use App\Entity\Trade;
use App\Form\TradeType;
use App\Repository\TokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\CallApiBinance;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


#[Route('/market')]
class MarketController extends AbstractController
{
    #[Route('/', name: 'market')]
    public function index(CallApiBinance $call, TokenRepository $tokenRepository): Response
    {
        $getTokens = $tokenRepository->findAll();
        $tokens = [];

        forEach($getTokens as $token) {
            $tokens[$token->getSlug()] = 0;
        }

        $prices = $call->getCurrentValue($tokens);
        
        forEach($getTokens as $value) {
            $nameToken = $value->getSlug();
            $value->setPrice($prices[$nameToken]);
        }
        
        return $this->render('market/index.html.twig', [
            'controller_name' => 'Market',
            'tokens' => $getTokens
        ]);
    }

    #[Route('/order', name: 'order')]
    public function buy(Request $request, TokenRepository $tokenRepository, EntityManagerInterface $entityManager): Response
    {
        // Trade proto
        $trade = new Trade();
        $form = $this->createForm(TradeType::class, $trade);

        if ($request->isMethod('post')) {
            // Manage of data
            $trade->setNumber($request->get('numberToken'));
            $trade->setTypeOrder($request->get('type'));
            $trade->setPrice(floatval($request->get('price')));
            $trade->setUser($this->getUser());
            $token = $tokenRepository->findOneBy(array('slug' => $request->request->get('token')));
            $trade->setToken($token);
            // Send data in DB
            $entityManager->persist($trade);
            $entityManager->flush();
        }
        
        // Redirection
        return $this->redirectToRoute('market');
    }


}
