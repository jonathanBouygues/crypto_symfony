////// PAGE MARKET

// Instanciation des variables
const formActive = document.querySelector('.formBuy');
const buttonBuy = document.querySelectorAll('.buttonBuy');
const tableToken = document.getElementById('tableToken');
const orderToken = document.getElementById('orderToken');
const orderPrice = document.getElementById('orderPrice');
const orderType = document.getElementById('orderType');

// Manage the buy path
for (const button of buttonBuy) {
    button.addEventListener('click', function() {
        // Div hidden
        formActive.classList.toggle("inactive");
        tableToken.classList.toggle("inactive");
        // Modify token & price of the form
        console.log(orderType);
        orderType.value = this.textContent.toLowerCase();
        orderPrice.value = this.parentElement.previousElementSibling.textContent;
        orderToken.value = this.parentElement.dataset.token;
    });
}

