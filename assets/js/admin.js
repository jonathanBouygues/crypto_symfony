// // PAGE ADMIN

// Instanciation des variables
const buttonUser = document.getElementById('buttonUser');
const buttonToken = document.getElementById('buttonToken');
const allToken = document.getElementById('allToken');
const allUser = document.getElementById('allUser');

// Manage the bloc user
buttonUser.addEventListener('click', function() {
    allToken.classList.add('inactive');
    allUser.classList.remove('inactive');
});

// Manage the bloc token
buttonToken.addEventListener('click', function() {
    allUser.classList.add('inactive');
    allToken.classList.remove('inactive');
});
