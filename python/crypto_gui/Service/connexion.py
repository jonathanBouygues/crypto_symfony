# Import standard module
import mysql.connector as mysql
import json
from argon2 import PasswordHasher, Type
import time
import urllib.request
# Import feat module
import Service.binance as binance
import Model.dto.market_dto as market_dto


class Connexion:
    
    def __init__(self) -> None:
        self.con = mysql.connect(host="localhost", user="root", password="root", database="crypto")

    # Call the table user to check the authentification
    def check_user_db(self, user) :
        # Get the data
        email = user.getEmail()
        password = user.getPassword()
        # Call the DB
        cursor = self.con.cursor(dictionary=True)
        cursor.execute("select * from user ") 
        users = cursor.fetchall()
        # Loop on all users
        for user in users :
            ph = PasswordHasher(memory_cost=65536, time_cost=3, type=Type.ID)
            if email == user["email"] and ph.verify(user["password"], password) :
                # Return user's data
                return user
        # Return empty if faled authentification
        return {}
    
    # By the symfony app',call all users to render on the app
    def call_users(self) :
        #  Call the api
        url = "http://localhost/api/users"
        contents = urllib.request.urlopen(url).read()
        # Manage the json data and return thses
        data = json.loads(contents)
        return data     
    
    # Insert a new user in DB 
    def insert_user(self, new_user) :
        #  Instantiate the varaibles
        user_email = new_user.getEmail()
        user_roles = new_user.getRole()
        user_password = new_user.getPassword()
        user_name = new_user.getName()
        user_firstname = new_user.getFirstname()
        # Call the DB
        cursor = self.con.cursor()
        # Bind parameters
        sql = "insert into user (email, roles, password, name, firstname, is_verified) VALUES (%s, %s, %s, %s, %s, 0)"
        val = (user_email, user_roles, user_password, user_name, user_firstname)
        # Prepare and send the request
        cursor.execute(sql, val)
        self.con.commit()
        
    def call_market(self) :
        # Call the DB
        cursor = self.con.cursor(dictionary=True)
        cursor.execute("select * from token ") 
        tokens = cursor.fetchall()
        
        # Create the objects
        datas_fusion = []
        for token in tokens:
            # Instantiate an object DTO
            tokens_dto = market_dto.MarketDto() 
            tokens_dto.setName(token['name'])
            tokens_dto.setSlug(token['slug'])
            tokens_dto.setCreated(token['created_at'])
            datas_fusion.append(tokens_dto)

        # Update the tokens with price
        tokens_priced = binance.callApi(datas_fusion)
        # Return the datas
        return tokens_priced
    
    def call_trades(self) :
        # call the DB
        cursor = self.con.cursor(dictionary=True)
        sql = "SELECT tr.id, u.email, tok.name, tr.number, tr.price, tr.type_order, tr.created_at \
                    FROM trade AS tr \
                    INNER JOIN user AS u ON u.id = tr.user_id \
                    INNER JOIN token AS tok ON tr.token_id = tok.id"
        cursor.execute(sql)
        trades = cursor.fetchall()
        # all trades
        return trades
    
    # By the symfony app',call all users to render on the app
    def call_charts(self) :
        #  Call the api
        url = "http://localhost/api/charts"
        contents = urllib.request.urlopen(url).read()
        # Manage the json data and return thses
        data = json.loads(contents)
        return data     
        
        
        
