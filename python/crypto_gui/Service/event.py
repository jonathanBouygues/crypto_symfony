# Import standard module
import tkinter as tk
from collections import defaultdict



subscribers = defaultdict(list)


def subscribe(event_type, func):
    subscribers[event_type].append(func)

def post_event(event_type, data=None):
    if event_type in subscribers:
        for fn in subscribers[event_type]:
            if data is None:
                fn()
            else:
                fn(data)
