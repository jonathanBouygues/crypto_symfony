import tkinter as tk
from tkinter import ttk
from collections import defaultdict

subscribers = defaultdict(list)


def subscribe(event_type, func):
    subscribers[event_type].append(func)


def post_event(event_type, data=None):
    if event_type in subscribers:
        for fn in subscribers[event_type]:
            if data is None:
                fn()
            else:
                fn(data)


LARGE_FONT = ("Verdana", 12)


def qf(quickPrint):
    print(quickPrint)


# MODEL
class User:
    def __init__(self):
        self.username = ""

    def set_username(self, username):
        self.username = username


# DTO for view
class UserLoginDto:
    def __init__(self):
        self.username = ""
        self.password = ""

    def set_username(self, username):
        self.username = username

    def set_password(self, password):
        self.password = password

    def get_username(self):
        return self.username


# Controller
class ControllerExample:
    def __init__(self):
        subscribe("user_login", self.login)
        subscribe("user_register", self.verifyInput)

    def login(self, user_dto):
        user = User()
        user.set_username(user_dto.get_username())
        qf(f"{user.username} has logged in")

    def verifyInput(self):
        qf("Input verified")


# VIEWS
class StartPage(tk.Frame):
    def __init__(self, parent, main_view):
        tk.Frame.__init__(self, parent)
        label = ttk.Label(self, text="""My Crypto App""", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button = ttk.Button(self, text="Login", command=lambda: main_view.show_frame(LoginPage))
        button.pack()

        button2 = ttk.Button(self, text="Register", command=lambda: main_view.show_frame(RegisterPage))
        button2.pack()



class LoginPage(tk.Frame):
    def __init__(self, parent, main_view):
        tk.Frame.__init__(self, parent)
        label = ttk.Label(self, text="Login", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home", command=lambda: main_view.show_frame(StartPage))
        button1.pack()

        button2 = ttk.Button(self, text="Connect", command=self.login)
        button2.pack()

    def login(self):
        userDto = UserLoginDto()
        userDto.set_username("Toto")
        userDto.set_password("mon_mdp")
        post_event("user_login", userDto)


class RegisterPage(tk.Frame):
    def __init__(self, parent, main_view):
        tk.Frame.__init__(self, parent)
        label = ttk.Label(self, text="Register", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="Back to Home", command=lambda: main_view.show_frame(StartPage))
        button1.pack()

        button2 = ttk.Button(self, text="Login", command=lambda: main_view.show_frame(LoginPage))
        button2.pack()

        button3 = ttk.Button(self, text="Register", command=self.register)
        button3.pack()

    def register(self):
        post_event("user_register")


# Main application
class MyApplication(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        # Init controllers
        ControllerExample()

        # define main window
        # tk.Tk.iconbitmap(self, default='./assets/icon.ico')
        tk.Tk.wm_title(self, "My Blockchain App")
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartPage, LoginPage, RegisterPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0,  sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


app = MyApplication()
app.mainloop()
