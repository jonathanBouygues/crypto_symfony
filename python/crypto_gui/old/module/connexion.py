# Import standard module
import mysql.connector as mysql
import json
import module.binance as binance


class Connexion:
    
    def __init__(self) -> None:
        self.con = mysql.connect(host="localhost", user="root", password="root", database="crypto")

    def callUser(self) :
        # Call the DB
        cursor = self.con.cursor(dictionary=True)
        cursor.execute("select * from user ") 
        users = cursor.fetchall()

        return users

    def insertUsers(self, user_email, user_password, user_name, user_firstname) :
        # Call the DB
        cursor = self.con.cursor()
        # Bind parameters
        sql = "insert into user (email, roles, password, name, firstname, is_verified) VALUES (%s, %s, %s, %s, %s, 0)"
        val = (user_email, json.dumps(["ROLE_USER"]), user_password, user_name, user_firstname)
        cursor.execute(sql, val)
        # Send the request
        self.con.commit()
        
    def callMarket(self) :
        # Call the DB
        cursor = self.con.cursor(dictionary=True)
        cursor.execute("select * from token ") 
        tokens = cursor.fetchall()
        # Update price
        tokens_priced = binance.callApi(tokens)
        # Return the datas
        return tokens_priced
    
    def callTrades(self) :
        # call the DB
        cursor = self.con.cursor(dictionary=True)
        sql = "SELECT tr.id, u.email, tok.name, tr.number, tr.price, tr.type_order, tr.created_at \
                    FROM trade AS tr \
                    INNER JOIN user AS u ON u.id = tr.user_id \
                    INNER JOIN token AS tok ON tr.token_id = tok.id"
        cursor.execute(sql)
        trades = cursor.fetchall()
        # all trades
        return trades
        
        
        
