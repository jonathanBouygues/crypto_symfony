import urllib.request
import json

# Update price token
def callApi(tokens):
    for index, token in enumerate(tokens):
        slug_mod = token['slug'].upper()
        url = f'https://api.binance.com/api/v3/avgPrice?symbol={ slug_mod }'
        contents = urllib.request.urlopen(url).read()
        # Manage the json response
        data_price = json.loads(contents)
        tokens[index]['price'] = round(float(data_price["price"]),2)
    # Return the data modified
    return tokens
    