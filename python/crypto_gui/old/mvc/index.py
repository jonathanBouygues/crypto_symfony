import tkinter as tk


# CONTROLLER
class Controller:
    def __init__(self, master):
        self.main_view = MainWindow(root)
        self.user_controller = UserController(self.main_view)

    def Display(self):
        self.main_view.mainloop()


class UserController:
    def __init__(self, root):
        self.user = User()
        self.main_view = MainWindow(root)
        self.login_view = self.main_view.login_view
        self.login_view.login_button.config(command=self.Login)

        self.main_view.DisplayLogin()

    def Login(self):
        self.user.username = "toto"
        print(self.user.username)
        # self.main_view.DisplayUserInView(self.user)


# MODEL
class User:
    def __init__(self):
        self.username = ""


# VUE
class MainWindow(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        # self.protocol('WM_DELETE_WINDOW', self.master.destroy)
        # create menu
        self.login_view = LoginView()

    def DisplayLogin(self):
        self.login_view.pack()


class LoginView(tk.Frame):
    def __init__(self):
        tk.Frame.__init__(self)
        self.login_button = tk.Button(self, text="Login", width=100)
        self.login_button.pack()


root = tk.Tk()
app = Controller(root)
app.Display()
