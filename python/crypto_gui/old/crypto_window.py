# Import standard module
from tkinter import *
from tkinter import messagebox
from argon2 import PasswordHasher, Type
import time
# Import feat module 
import module.connexion as con
import module.fullscreen as fs


class MyApp:
    def __init__(self) -> None:
        # Create my basic window
        self.window = Tk()
        self.window.title('Crypto appli')
        self.window.minsize(width=300,height=300)
        self.window.config(background="grey")
        self.window.resizable(height=False,width=False)
        # Formate the window
        app = fs.FullScreenApp(self.window)    
        # Call home composant
        self.start_appli()
    
    def start_appli(self) :
        # Create home frames
        self.container = Frame(self.window, bg="white", width=600, height=600, pady="10", padx="10")
        # Create photo
        self.photo = PhotoImage(file='//wsl$/Ubuntu-20.04/home/jonathan/crypto_symfony/python/crypto_gui/images/ledger.png')
        self.logo = Label(self.container, image=self.photo, pady=30).grid(row=0, column=0, columnspan=2)
        self.title = Label(self.container, text="Bienvenue chez Ledger", pady=20).grid(row=1, column=0, columnspan=2)
        # Call home composant
        # Button Login
        self.buttonLogin = Button(self.container, text="Login",height=3, bd=5, width=20, font=("Arial", 20), bg="white", fg="black", command=self.container_login).grid(row=2,column=0)
        # Button Register
        self.buttonRegister = Button(self.container, text="Register",height=3, bd=5, width=20, font=("Arial", 20), bg="white", fg="black",padx="5", command=self.container_register).grid(row=2,column=1)
        # Render home frames
        self.container.pack(expand=YES)
        
    def container_register(self) :
        # Delete the home container
        self.container.destroy()
        # Global frame
        self.register_frame = Frame(self.window, bg="white", height=500, width=500)
        # Email
        self.register_label_email = Label(self.register_frame, text="Email", bg="white",width=10, height=5).grid(row=0,column=0)
        self.register_input_email = Entry(self.register_frame, bg="white", font=('Arial', 20), fg="black")
        self.register_input_email.grid(row=0,column=1)
        # Space
        self.register_label_space = Label(self.register_frame, text="", bg="white", width=10, height=5).grid(row=1,column=1)
        #  Password
        self.register_label_password = Label(self.register_frame, text="Password", bg="white", height=5, width=10).grid(row=2,column=0)
        self.register_input_password = Entry(self.register_frame, show='*', bg="white", font=('Arial', 20), fg="black")
        self.register_input_password.grid(row=2,column=1)
        # Space
        self.register_label_space2 = Label(self.register_frame, text="", bg="white", width=10, height=5).grid(row=3,column=1)
        #  NAme
        self.register_label_name = Label(self.register_frame, text="Name", bg="white", height=5, width=10).grid(row=4,column=0)
        self.register_input_name = Entry(self.register_frame, bg="white", font=('Arial', 20), fg="black")
        self.register_input_name.grid(row=4,column=1)
        # Space
        self.register_label_space3 = Label(self.register_frame, text="", bg="white", width=10, height=5).grid(row=5,column=1)
        #  Password
        self.register_label_firstname = Label(self.register_frame, text="Firstname", bg="white", height=5, width=10).grid(row=6,column=0)
        self.register_input_firstname = Entry(self.register_frame, bg="white", font=('Arial', 20), fg="black")
        self.register_input_firstname.grid(row=6,column=1)
        # button validate
        self.register_validate = Button(self.register_frame, text="register", command=self.register_check).grid(row=7, column=1)
        # Render frame
        self.register_frame.pack(expand=YES)

    def container_login(self) :
        # Delete the home container
        self.container.destroy()
        # Global frame
        self.login_frame = Frame(self.window, bg="white", height=500, width=500)
        # Name
        self.login_label_name = Label(self.login_frame, text="Name", bg="white",width=10, height=5).grid(row=0,column=0)
        self.login_input_name = Entry(self.login_frame, bg="white", font=('Arial', 20), fg="black")
        self.login_input_name.grid(row=0,column=1)
        # Space
        self.login_label_space = Label(self.login_frame, text="", bg="white", width=10, height=5).grid(row=1,column=1)
        #  Password
        self.login_label_password = Label(self.login_frame, text="Password", bg="white", height=5, width=10).grid(row=2,column=0)
        self.login_input_password = Entry(self.login_frame, bg="white", show='*', font=('Arial', 20), fg="black")
        self.login_input_password.grid(row=2,column=1)
        # button validate
        self.login_validate = Button(self.login_frame, text="Login", command=self.login_check).grid(row=3, column=1)
        # Render the frame
        self.login_frame.pack(expand=YES)
        
    def register_check(self) :
        # Data's instanciation
        user_email = self.register_input_email.get()
        user_name = self.register_input_name.get()
        user_firstname = self.register_input_firstname.get()
        ph = PasswordHasher(memory_cost=65536, time_cost=3, type=Type.ID)
        user_password = ph.hash(self.register_input_password.get())
        
        # Call the DB and insert data
        if user_name.isalpha() and user_firstname.isalpha() and user_email.isascii() :
            try:
                send_data = con.Connexion()
            except Exception as error:
                # Error format datas  
                messagebox.showerror('Erreur', f'Erreur : {error}')
            else:
                send_data.insertUsers(user_email, user_password, user_name, user_firstname)
                # Error format datas  
                messagebox.showinfo('User saved', 'Your profil is really saved')
        else :
            # Error format datas  
            messagebox.showerror('Format data\'s false', 'Your datas doesn\'t have the correct format')
            
    def login_check(self) :
        # Get the input)
        user_name = self.login_input_name.get()
        user_password = self.login_input_password.get()
        # Call users
        try:
            self.call_db = con.Connexion()
        except Exception as error:
            # Error format datas  
            messagebox.showerror('Erreur', f'Erreur : {error}')
        else:           
            self.users = self.call_db.callUser()
        # Condition
        for user in self.users :
            ph = PasswordHasher(memory_cost=65536, time_cost=3, type=Type.ID)
            if user_name == user["email"] and ph.verify(user["password"], user_password) :
                # Delete my login container and create ma action container
                self.login_frame.destroy()
                self.action_frame = Frame(self.window, bg="white", height=500, width=500)
                self.action_frame.grid(padx=10, pady=10)
                # Create menu   
                menu_crypto = Menu(self.window)
                self.window.config(menu=menu_crypto)
                menu_crypto.add_cascade(label="Users", command=self.render_users)
                menu_crypto.add_cascade(label="Market", command=self.render_market)
                menu_crypto.add_cascade(label="Trades", command=self.render_trades)
                menu_crypto.add_cascade(label="Quitter", command=self.exit_app)
                # Stop the method  
                return None

        # Error ID/password   
        messagebox.showinfo('Id/Password error', 'Your id/email is false or unknown !')

    # Create method's Menu Users
    def render_users(self):
        # clean the datas 
        self.destroy_widget()
        # New Title
        users_title = Label(self.action_frame, text="All users")
        users_title.grid(row=0,column=0, columnspan=3, pady=30)
        # Render datas
        for idx, user in enumerate(self.users):
            login_label_email = Label(self.action_frame, text=user["email"], bg="white", width=30, height=5).grid(row=idx+1,column=0)
            login_label_name = Label(self.action_frame, text=user["name"], bg="white",width=30, height=5, bd=2).grid(row=idx+1,column=1)
            login_label_firstname = Label(self.action_frame, text=user["firstname"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=2)

    # Create method's Menu Market
    def render_market(self):
        # clean the datas 
        self.destroy_widget()
        # New title
        market_title = Label(self.action_frame, text="All market").grid(row=0,column=0, columnspan=3, pady=30)
        market_button_create = Button(self.action_frame, text="Create", command=self.container_token).grid(row=1, column=3)
        # Call datas
        tokens = self.call_db.callMarket()
        # Render datas
        for idx, user in enumerate(tokens):
            market_label_name = Label(self.action_frame, text=user["name"], bg="white", width=30, height=5).grid(row=idx+2,column=0)
            market_label_slug = Label(self.action_frame, text=user["slug"], bg="white",width=30, height=5, bd=2).grid(row=idx+2,column=1)
            market_label_created = Label(self.action_frame, text=user["created_at"], bg="white",width=30, height=5, bd=2).grid(row=idx+2, column=2)    
            market_label_price = Label(self.action_frame, text=user["price"], bg="white",width=30, height=5, bd=2).grid(row=idx+2, column=3)

    # Create method's Menu Trades
    def render_trades(self):
        # clean the datas 
        self.destroy_widget()
        # New title
        trade_title = Label(self.action_frame, text="All trades").grid(row=0,column=0, columnspan=3, pady=30)
        # Call trades
        trades = self.call_db.callTrades()
        # Render datas 
        for idx, trade in enumerate(trades):
            trade_label_id = Label(self.action_frame, text=trade["id"], bg="white", width=30, height=5).grid(row=idx+1,column=0)
            trade_label_email = Label(self.action_frame, text=trade["email"], bg="white",width=30, height=5, bd=2).grid(row=idx+1,column=1)
            trade_label_token = Label(self.action_frame, text=trade["name"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=2)    
            trade_label_number = Label(self.action_frame, text=trade["number"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=3)     
            trade_label_price = Label(self.action_frame, text=trade["price"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=3)  
            trade_label_type = Label(self.action_frame, text=trade["type_order"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=3)  
            trade_label_created = Label(self.action_frame, text=trade["created_at"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=3)  
                            
    # Create method's Menu Exit
    def exit_app(self):
        msg_box = messagebox.askquestion('Exit', 'Do you want really quit the appli ?', icon='error')
        if msg_box == 'yes':
             # Wait 10 seconds for foo
            messagebox.showinfo('Close', 'The app is going to close in 2 seconds')
            time.sleep(2)
            self.window.destroy()
        else :
            messagebox.showinfo('Welcome back', 'Welcome back !')
    
    # Destroy data
    def destroy_widget(self):
        for widget in self.action_frame.winfo_children():
            widget.destroy()
            
    # Create token
    def container_token(self):
        # Clean the frame 
        self.destroy_widget()
        # Title
        create_token_title = Label(self.action_frame, text="Create token").grid(row=0, column=0)
        # Formulaire
        create_token_label_name = Label(self.action_frame, text="Name").grid(row=1, column=0)
        self.create_token_input_name = Entry(self.action_frame)
        self.create_token_input_name.grid(row=1, column=1)
        create_token_label_slug = Label(self.action_frame, text="Slug").grid(row=2, column=0)
        self.create_token_input_slug = Entry(self.action_frame)
        self.create_token_input_slug.grid(row=2, column=1)
        create_token_button = Button(self.action_frame, text="Save", command=self.token_check).grid(row=3, column=0)
        
    # Check data's token before insert
    def token_check(self) :
        print(self.create_token_input_name)
        print(self.create_token_input_slug)
        
        try :
            self.call_db.callUser()
        except Exception as error:
            # Error format datas  
            messagebox.showerror('Erreur', f'Erreur : {error}')
        else:
            self.call_db.insertToken()
        

# Call at beginning
if __name__ == '__main__':
    # Run the app
    app = MyApp()
    app.window.mainloop()
