#  Import standard module
import json


class User():
    def __init__(self) :
        self.__email = ""
        self.__roles =  json.dumps(["ROLE_USER"])
        self.__password = ""
        self.__name = ""
        self.__firstname = ""
        self.__is_verified = 0
        
    def setEmail(self, email):
        self.__email = email
        
    def setPassword(self, password):
        self.__password = password
        
    def setName(self, name):
        self.__name = name
        
    def setFirstname(self, firstname):
        self.__firstname = firstname
        
    def getEmail(self):
        return self.__email
        
    def getPassword(self):
        return self.__password
        
    def getName(self):
        return self.__name
        
    def getFirstname(self):
        return self.__firstname
    
    def getRoles(self):
        return self.__roles
        