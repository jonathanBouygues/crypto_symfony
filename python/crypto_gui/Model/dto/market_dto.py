# Create DTO for check the user's authentification 
class MarketDto():
    def __init__(self) -> None:
        self.__name = ""
        self.__slug = ""
        self.__created = ""
        self.__price = ""
        
    def setName(self, name):
        self.__name = name
        
    def setSlug(self, slug):
        self.__slug = slug
        
    def getName(self):
        return self.__name
        
    def getSlug(self):
        return self.__slug
    
    def setCreated(self, created):
        self.__created = created
        
    def setPrice(self, price):
        self.__price = price
        
    def getCreated(self):
        return self.__created
        
    def getPrice(self):
        return self.__price