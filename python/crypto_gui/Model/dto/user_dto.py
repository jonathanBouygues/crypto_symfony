# Create DTO for check the user's authentification 
class UserDto():
    def __init__(self) -> None:
        self.__email = ""
        self.__password = ""
        
    def setEmail(self, email):
        self.__email = email
        
    def setPassword(self, password):
        self.__password = password
        
    def getEmail(self):
        return self.__email
        
    def getPassword(self):
        return self.__password