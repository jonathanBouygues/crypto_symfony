# Import standard module
import tkinter as tk

        
class RegisterView(tk.Frame):
    def __init__(self, parent):
        # Global frame
        self.frame = tk.Frame(parent, name='register_view', bg="white", height=500, width=500)
        self.parent = parent
        # Email
        self.register_label_email = tk.Label(self.frame, text="Email", bg="white",width=10, height=5).grid(row=0,column=0)
        self.register_input_email = tk.Entry(self.frame, bg="white", font=('Arial', 20), fg="black")
        self.register_input_email.grid(row=0,column=1)
        # Space
        self.register_label_space = tk.Label(self.frame, text="", bg="white", width=10, height=5).grid(row=1,column=1)
        #  Password
        self.register_label_password = tk.Label(self.frame, text="Password", bg="white", height=5, width=10).grid(row=2,column=0)
        self.register_input_password = tk.Entry(self.frame, show='*', bg="white", font=('Arial', 20), fg="black")
        self.register_input_password.grid(row=2,column=1)
        # Space
        self.register_label_space2 = tk.Label(self.frame, text="", bg="white", width=10, height=5).grid(row=3,column=1)
        #  NAme
        self.register_label_name = tk.Label(self.frame, text="Name", bg="white", height=5, width=10).grid(row=4,column=0)
        self.register_input_name = tk.Entry(self.frame, bg="white", font=('Arial', 20), fg="black")
        self.register_input_name.grid(row=4,column=1)
        # Space
        self.register_label_space3 = tk.Label(self.frame, text="", bg="white", width=10, height=5).grid(row=5,column=1)
        #  Password
        self.register_label_firstname = tk.Label(self.frame, text="Firstname", bg="white", height=5, width=10).grid(row=6,column=0)
        self.register_input_firstname = tk.Entry(self.frame, bg="white", font=('Arial', 20), fg="black")
        self.register_input_firstname.grid(row=6,column=1)
        # button validate
        self.register_validate = tk.Button(self.frame, text="register", command="")
        self.register_validate.grid(row=7,column=1)
    
    def display_frame(self):
        self.frame.pack()
