# Import standard module
import tkinter as tk
        
class StartView(tk.Frame):
    def __init__(self, parent):
        self.frame = tk.Frame(parent)
        self.parent = parent
        self.frame.config(bg="yellow",border=5, width=500, height=500)
        # Create photo
        self.photo = tk.PhotoImage(file='//wsl$/Ubuntu-20.04/home/jonathan/crypto_symfony/python/crypto_gui/Asset/ledger.png')
        self.logo = tk.Label(self.frame, image=self.photo, pady=30).grid(row=0, column=0, columnspan=2)
        self.title = tk.Label(self.frame, text="Bienvenue chez Ledger", pady=20).grid(row=1, column=0, columnspan=2)
        # Call home composant
        # Button Login
        self.button_login = tk.Button(self.frame, text="Login",height=3, bd=5, width=20, font=("Arial", 20), bg="white", fg="black")
        self.button_login.grid(row=2,column=0)
        # Button Register
        self.button_register = tk.Button(self.frame, text="Register",height=3, bd=5, width=20, font=("Arial", 20), bg="white", fg="black",padx="5")
        self.button_register.grid(row=2,column=1)

    def display_start(self):
        self.frame.pack()
