# Import standard module
import tkinter as tk

        
class LoginView(tk.Frame):
    def __init__(self, parent):
        self.frame = tk.Frame(parent, name="login_view")
        self.parent = parent
        self.frame.config(bg="pink", border=5, width=500, height=500)
        # Name
        self.login_label_email = tk.Label(self.frame, text="Email", bg="white",width=10, height=5).grid(row=0,column=0)
        self.login_input_email = tk.Entry(self.frame, bg="white", font=('Arial', 20), fg="black")
        self.login_input_email.grid(row=0,column=1)
        # Space
        self.login_label_space = tk.Label(self.frame, text="", bg="white", width=10, height=5).grid(row=1,column=1)
        #  Password
        self.login_label_password = tk.Label(self.frame, text="Password", bg="white", height=5, width=10).grid(row=2,column=0)
        self.login_input_password = tk.Entry(self.frame, bg="white", show='*', font=('Arial', 20), fg="black")
        self.login_input_password.grid(row=2,column=1)
        # button validate
        self.login_validate = tk.Button(self.frame, text="Login")
        self.login_validate.grid(row=3, column=1)
        
    def display_frame(self):
        self.frame.pack()