# Import standard module
import tkinter as tk

        
class UsersView(tk.Frame):
    def __init__(self, parent):
        self.frame = tk.Frame(parent, name="users_view")
        self.parent = parent
        # New Title
        self.users_title = tk.Label(self.frame, text="All users")
        self.users_title.grid(row=0,column=0, columnspan=3, pady=30)
        
    def display_frame(self, users):
        # Render datas
        for idx, user in enumerate(users):
            self.login_label_email = tk.Label(self.frame, text=user["email"], bg="white", width=30, height=5).grid(row=idx+1,column=0)
            self.login_label_name = tk.Label(self.frame, text=user["name"], bg="white",width=30, height=5, bd=2).grid(row=idx+1,column=1)
            self.login_label_firstname = tk.Label(self.frame, text=user["firstname"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=2)
        # And render the frame
        self.frame.pack()