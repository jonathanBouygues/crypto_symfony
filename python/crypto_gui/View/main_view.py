# Import standard module
import tkinter as tk
# Import feat module
import View.user.start_view as start_view
import View.user.login_view as login_view
import View.user.register_view as register_view
import View.user.users_view as users_view
import View.market.market_view as market_view
import View.trades.trades_view as trades_view
import View.market.charts_view as charts_view


class MainWindow(tk.Frame):
    def __init__(self, master=None):
        self.frame = tk.Frame(master, width=600, height=400, bg="pink")
        self.frame.grid()
        self.master = master
        #  Create the secondary frame
        self.start_view = start_view.StartView(self.frame)
        self.login_view = login_view.LoginView(self.frame)
        self.register_view = register_view.RegisterView(self.frame)
        self.users_view = users_view.UsersView(self.frame)
        self.market_view = market_view.MarketView(self.frame)
        self.trades_view = trades_view.TradesView(self.frame)
        self.charts_view = charts_view.ChartsView(self.frame)
        # Call the start frame
        self.display_start()

    def display_start(self):
        self.start_view.display_start()
        
    def destroy_children(self, name="default"):
        for widget in self.frame.winfo_children():
            if name not in str(widget) :
                widget.pack_forget()

    def test(self):
        print('yest')