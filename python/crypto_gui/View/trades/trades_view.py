# Import standard module
import tkinter as tk

        
class TradesView(tk.Frame):
    def __init__(self, parent):
        self.frame = tk.Frame(parent, name="trades_view")
        self.parent = parent
        self.frame.config(bg="pink", border=5, width=500, height=500)
        # New title
        market_title = tk.Label(self.frame, text="All trades")
        market_title.grid(row=0,column=0, columnspan=3, pady=30)


    def display_frame(self, trades):
        for idx, trade in enumerate(trades):
            self.trade_label_id = tk.Label(self.frame, text=trade["id"], bg="white", width=30, height=5).grid(row=idx+1,column=0)
            self.trade_label_email = tk.Label(self.frame, text=trade["email"], bg="white",width=30, height=5, bd=2).grid(row=idx+1,column=1)
            self.trade_label_token = tk.Label(self.frame, text=trade["name"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=2)    
            self.trade_label_number = tk.Label(self.frame, text=trade["number"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=3)     
            self.trade_label_price = tk.Label(self.frame, text=trade["price"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=3)  
            self.trade_label_type = tk.Label(self.frame, text=trade["type_order"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=3)  
            self.trade_label_created = tk.Label(self.frame, text=trade["created_at"], bg="white",width=30, height=5, bd=2).grid(row=idx+1, column=3)  
        # Pump the frame
        self.frame.pack()