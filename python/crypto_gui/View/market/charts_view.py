# Import standard module
import tkinter as tk
from pandas import DataFrame
from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class ChartsView(tk.Frame):
    def __init__(self, parent):
        self.frame = tk.Frame(parent, name="charts_view")
        self.parent = parent
        self.frame.config(bg="pink", border=5, width=500, height=500)
        
    def display_frame(self, charts):
        # Loop on the tokens
        datas = {}
        # Change the format
        for chart in charts:
            # Check if property exists
            if not chart["symbol"] in datas.keys() :
                datas[chart["symbol"]] = {'date': [], 'price': []}
            # Store the datas
            datas[chart["symbol"]]['date'].append(chart['date'])
            datas[chart["symbol"]]['price'].append(chart['price'])

        # Create the data frame form pandas & matplotlib
        for key, value in datas.items() :  
            df = DataFrame(value,columns=['date','price'])
            figureChart = plt.Figure(figsize=(6,6), dpi=100)
            ax1 = figureChart.add_subplot(111)
            bar = FigureCanvasTkAgg(figureChart, self.parent)
            bar.get_tk_widget().pack(side=tk.LEFT, fill=tk.BOTH)
            df = df[['date','price']].groupby('date').sum()
            df.plot(kind='line', legend=True, ax=ax1)
            ax1.set_title(key)
