# Import standard module
import tkinter as tk

        
class MarketView(tk.Frame):
    def __init__(self, parent):
        self.frame = tk.Frame(parent, name="market_view")
        self.parent = parent
        self.frame.config(bg="pink", border=5, width=500, height=500)
        # New title
        market_title = tk.Label(self.frame, text="All market")
        market_title.grid(row=0,column=0, columnspan=3, pady=30)


    def display_frame(self, tokens):
        # Loop on the tokens
        for idx, token in enumerate(tokens):
            self.market_label_name = tk.Label(self.frame, text=token.getName(), bg="white", width=30, height=5).grid(row=idx+2,column=0)
            self.market_label_slug = tk.Label(self.frame, text=token.getSlug(), bg="white",width=30, height=5, bd=2).grid(row=idx+2,column=1)
            self.market_label_created = tk.Label(self.frame, text=token.getCreated(), bg="white",width=30, height=5, bd=2).grid(row=idx+2, column=2)    
            self.market_label_price = tk.Label(self.frame, text=token.getPrice(), bg="white",width=30, height=5, bd=2).grid(row=idx+2, column=3)
        # Pump the frame
        self.frame.pack()