# import standard module
import tkinter as tk
from tkinter import messagebox
from argon2 import PasswordHasher, Type
import re
# Import feat module
import Service.connexion as con
import Service.event as event
import Model.dto.user_dto as user_dto
import Model.user as user

class UserController():
    def __init__(self, master=None) -> None:
        # Call the main window
        self.master = master
        # Config command button
        self.master.start_view.button_login.config(command=self.form_login)
        self.master.start_view.button_register.config(command=self.form_register)
        self.master.login_view.login_validate.config(command=self.verify_user)
        self.master.register_view.register_validate.config(command=self.check_register)
    
    # Login management (form)
    def form_login(self):
        # Destroy the child
        self.master.destroy_children('login_view')
        # call the frame
        self.master.login_view.display_frame()
    # Login management (identification verification)
    def verify_user(self):
        # Instantiate an object DTO
        user_to_verify = user_dto.UserDto()
        # Set the data
        user_to_verify.setEmail(self.master.login_view.login_input_email.get())
        user_to_verify.setPassword(self.master.login_view.login_input_password.get())
        # Call users
        try:
            self.call_db = con.Connexion()
        except Exception as error:
            # Error format datas  
            messagebox.showerror('Erreur', f'Erreur : {error}')
        else:           
            self.user = self.call_db.check_user_db(user_to_verify)
        # Call the connexion and check the user
        if len(self.user) != 0:
            self.master.destroy_children()
            event.post_event("user_verified", self.user)
        else:
            messagebox.showerror('Credentials error', 'Your credentials are false')
            
    # Registry management (form)
    def form_register(self):
        # Destroy the child
        self.master.destroy_children('register_view')
        # Call the frame
        self.master.register_view.display_frame()
    # Registry management (management of user's data)
    def check_register(self):
        # Instantiate an user object
        new_user = user.User()
        # Get the datas
        new_user.setEmail(self.master.register_view.register_input_email.get())
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        new_user.setName(self.master.register_view.register_input_name.get())
        new_user.setFirstname(self.master.register_view.register_input_firstname.get())
        ph = PasswordHasher(memory_cost=65536, time_cost=3, type=Type.ID)
        new_user.setPassword(ph.hash(self.master.register_view.register_input_password.get()))
        
        # Call the DB and insert data
        if new_user.getName().isalpha() and new_user.getFirstname().isalpha() and re.fullmatch(regex,new_user.getEmail()) :
            try:
                call_db = con.Connexion()
            except Exception as error:
                # Error format datas  
                messagebox.showerror('Erreur', f'Erreur : {error}')
            else:
                call_db.insert_user(new_user)
                # Error format datas  
                messagebox.showinfo('User saved', 'Your profil is really saved')
                # Destroy the child
                self.master.destroy_children()
        else :
            # Error format datas  
            messagebox.showerror('Format data\'s false', 'Your datas doesn\'t have the correct format')

    # Render all the users for the admin
    def renderUsers(self):
        # Destroy the child
        self.master.destroy_children('users_view')
        # Call all user of db  
        try:
            self.call_db = con.Connexion()
        except Exception as error:
            # Error format datas  
            messagebox.showerror('Erreur', f'Erreur : {error}')
        else:           
            self.users = self.call_db.call_users()
        # Call the frame
        self.master.users_view.display_frame(self.users)