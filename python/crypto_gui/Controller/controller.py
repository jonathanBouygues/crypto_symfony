from logging import root
import tkinter as tk
# Import feat module
import View.main_view as main
import Controller.user_controller as user
import Controller.market_controller as market
import Controller.trade_controller as trade
import Service.event as event


class Controller():
    def __init__(self, root):
        # Call the main window
        self.main_view = main.MainWindow(root)
        # Call the main window
        self.user_con = user.UserController(self.main_view)
        self.market_con = market.MarketController(self.main_view)
        self.trade_con = trade.TradeController(self.main_view)
        # Call the menu
        self.menu = self.create_menu(root)
        # Init the event
        event.subscribe("user_verified", self.activeMenu)
        
            
    def create_menu(self, root):
        # Create menu   
        self.menu_crypto = tk.Menu(root)
        root.config(menu=self.menu_crypto)
        self.user_menu = tk.Menu(self.menu_crypto)
        self.menu_crypto.add_cascade(label="Users", menu=self.user_menu)
        self.market_menu = tk.Menu(self.menu_crypto)
        self.menu_crypto.add_cascade(label="Market", menu=self.market_menu)
        self.trades_menu = tk.Menu(self.menu_crypto)
        self.menu_crypto.add_cascade(label="Trades", menu=self.trades_menu)
        self.menu_crypto.add_cascade(label="Quitter", command=root.quit)
            
    def activeMenu(self, data):
        self.user_menu.add_command(label="All users", command=self.user_con.renderUsers)
        self.market_menu.add_cascade(label="Current price", command=self.market_con.renderMarket)
        self.market_menu.add_cascade(label="Charts", command=self.market_con.renderCharts)
        self.trades_menu.add_cascade(label="Trades done", command=self.trade_con.renderTrades)