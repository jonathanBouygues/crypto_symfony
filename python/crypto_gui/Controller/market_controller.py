# import standard module
import tkinter as tk
from tkinter import messagebox
# Import feat module
import Service.connexion as con
import Service.event as event

class MarketController():
    def __init__(self, master=None) -> None:
        # Call the main master
        self.master = master
    
    def renderMarket(self):
        # Destroy the child
        self.master.destroy_children('market_view')
        # Call all tokens priced  
        try:
            self.call_db = con.Connexion()
        except Exception as error:
            # Error format datas  
            messagebox.showerror('Erreur', f'Erreur : {error}')
        else:           
            self.tokens = self.call_db.call_market()
        # Call the frame
        self.master.market_view.display_frame(self.tokens)
        
    def renderCharts(self):
        # Destroy the child
        self.master.destroy_children('charts_view')
        # Call the charts
        try:
            self.call_db = con.Connexion()
        except Exception as error:
            # Error format datas  
            messagebox.showerror('Erreur', f'Erreur : {error}')
        else:           
            self.charts = self.call_db.call_charts()
        # Call the frame with data
        self.master.charts_view.display_frame(self.charts)