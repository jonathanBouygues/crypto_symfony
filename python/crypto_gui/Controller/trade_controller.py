# import standard module
import tkinter as tk
from tkinter import messagebox
# Import feat module
import Service.connexion as con
import Service.event as event


class TradeController():
    def __init__(self, master=None) -> None:
        # Call the main master
        self.master = master
    
    
    def renderTrades(self):
        # Destroy the child
        self.master.destroy_children('trades_view')
        # Call all tokens priced  
        try:
            self.call_db = con.Connexion()
        except Exception as error:
            # Error format datas  
            messagebox.showerror('Erreur', f'Erreur : {error}')
        else:           
            self.tokens = self.call_db.call_trades()
        # Call the frame
        self.master.trades_view.display_frame(self.tokens)