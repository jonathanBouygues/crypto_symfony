# Import standard module
import tkinter as tk
# Import feat module
import Controller.controller as user
import Service.fullscreen as fs


# Call at beginning
if __name__ == '__main__':
    # Initiation of the app
    root = tk.Tk()
    root.title("Man")
    root.minsize(width=300,height=300)
    root.config(background="grey")
    root.resizable(height=False,width=False)
    app = user.Controller(root)
    # Formate the window
    app = fs.FullScreenApp(root)
    root.mainloop()
