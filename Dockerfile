FROM php:8.1.0-apache as base

# Some variables
ARG ENV_FILE=.env
ENV ENV_FILE $ENV_FILE
ENV USER application
ENV GROUP application
ENV APACHE_DOCUMENT_ROOT /var/www/html/public

# Create user and group with ID 1000 (to execute commands with the same ID as host user in container and avoid permissions problems)
RUN groupadd -g 1000 ${GROUP}
RUN useradd --create-home -u 1000 -s /bin/bash -g 1000 ${USER}

# Replace default DocumentRoot to our project folder in Apache vhosts
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Timezone
RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Fix bug since Debian 10 Buster when installing java package (https://github.com/geerlingguy/ansible-role-java/issues/64#issuecomment-393299088)
RUN mkdir -p /usr/share/man/man1

# Install some packages
RUN apt-get update \
    && apt-get install -y --no-install-recommends locales apt-utils git libicu-dev g++ libpng-dev libxml2-dev libzip-dev libonig-dev libxslt-dev

# PHP extensions
RUN docker-php-ext-install pdo pdo_mysql gd opcache intl zip calendar dom mbstring zip gd xsl xml

# Add needed python extensions
RUN pip install argon2-cffi

# Generate self-signed certificate
RUN set -x && openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certs/ssl-cert-snakeoil.pem -subj "/C=FR/ST=France/L=France/O=Security/OU=Development/CN=ent.mds.test"

# Enable default-ssl vhost
RUN a2ensite default-ssl

# Enable some Apache modules
RUN a2enmod rewrite negotiation headers ssl

# Install NodeJS
RUN set -x && curl --silent --location https://deb.nodesource.com/setup_14.x | bash -
RUN set -x && apt-get install -y nodejs

# Copy composer binary file from official docker image into our container
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Add Composer bin folder in PATH system variable to execute composer packages binary files without typing full path
RUN echo 'export PATH="$PATH:$HOME/.composer/vendor/bin"' >> /etc/bash.bashrc

# Jump to web folder into container
WORKDIR /var/www/html

# Clean container
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Extra for dev environment
FROM base as base_dev

# Install xdebug
RUN pecl install xdebug-3.1.2 && docker-php-ext-enable xdebug
